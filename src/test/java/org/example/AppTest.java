package org.example;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * Unit test for simple App.
 */
public class AppTest {

    @Test
    public void myTest() {

        int a = 0;
        int b = 0;

        Assert.assertEquals(a, b);

    }

    @Test
    public void stringCmpTest () {

        String a = "Hello";
        String b = new String(" World");

        Assert.assertEquals("Hello World", a + b);
    }

    @Test
    public void exceptionTest() {
        List<Integer> list = new ArrayList<>();
        Assert.assertTrue(list.isEmpty());
        Assert.assertEquals(0, list.size());

        list.add(10);
        System.out.println(list);

        Assert.assertFalse(list.isEmpty());
        Assert.assertEquals(1, list.size());

        Assert.assertEquals(Arrays.asList(10), list);

        try {
            list.get(5);
            Assert.fail();
        } catch (IndexOutOfBoundsException ex) {
            System.out.println("get returned exception");
        }
    }
}
