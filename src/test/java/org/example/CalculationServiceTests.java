package org.example;

import org.junit.Assert;
import org.junit.Test;

public class CalculationServiceTests {

    private CalculationService service = new CalculationService();

    @Test
    public void sumPositiveDigits() {

        Assert.assertEquals(10, service.sum(5,5));
        Assert.assertEquals(5, service.sum(5,0));
        Assert.assertEquals(5, service.sum(0,5));
    }

    @Test
    public void sumNegativeDigits() {

        Assert.assertEquals(0, service.sum(5,-5));
        Assert.assertEquals(-5, service.sum(-5,0));
        Assert.assertEquals(-5, service.sum(0,-5));

        Assert.assertEquals(1, service.sum(6, -5));
    }

    @Test
    public void divTests() {

        Assert.assertEquals(5, service.div(25,5));
        Assert.assertEquals(2, service.div(5,2));
        Assert.assertEquals(0, service.div(1,2));
    }
    @Test
    public void exceptionTests() {

        try {
            service.div(2, 0);
            Assert.fail();
        } catch (IllegalArgumentException e) {
            System.out.println("Exception caught error=" + e.toString());
        }
    }

}
