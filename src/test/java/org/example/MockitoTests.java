package org.example;

import org.junit.Test;
import org.mockito.Mockito;

import java.util.List;

public class MockitoTests {

    @Test
    public void test() {
        List<String> listMock = Mockito.mock(List.class);

        Mockito.when(listMock.get(Mockito.anyInt())).thenReturn("Hello");
//        Mockito.when(listMock.get(1)).thenReturn(" World");

        System.out.println("isEmpty:" + listMock.isEmpty());

        listMock.add("1");
        listMock.add("2");

        System.out.println("list.get(0)" + listMock.get(0));
        System.out.println("list.get(1)" + listMock.get(1));
    }
}
